'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
// const Factory = use('Factory')

// Factory.blueprint('App/Models/User', (faker) => {
//   return {
//     username: faker.username()
//   }
// })

const Factory = use('Factory')
const Hash = use('Hash')
const Faker = require('faker-br')
// sets locale to de
Faker.locale = "pt_BR";


Factory.blueprint('App/Models/User', async (faker) => {
  const genders  = [ 'MASCULINO' , 'FEMININO' ];
  const roles = ['PATIENT', 'ADMIN', 'DOCTOR']
  return {
    email: faker.email(),
    password: await Hash.make(faker.password()),
    name: Faker.name.firstName,
    cpf : await Faker.br.cpf,
    genre: await Faker.random.arrayElement(genders),
    role : await Faker.random.arrayElement(roles),
    contact : await Faker.phone.phoneNumber,
    img_profile : await Faker.internet.avatar,
    address_zipcode : await Faker.address.zipCode,
    address_street : await Faker.address.streetName ,
    address_number : await Faker.random.number,
    address_complement: await Faker.address.secondaryAddress,
    address_city: await Faker.address.city,
    address_uf: "RJ",
    address_neighborhood: await Faker.address.city,
    accept_terms: true
  }
})
Factory.blueprint('App/Models/AcademicEducation', async (faker) => {

  return {
    institute_location: await Faker.address.streetName,
    institute_conclusion : await Faker.date.between('1990-01-01', '2020-04-01'),
    residence_location : await Faker.address.streetName,
    residence_conclusion: await Faker.date.between('1990-01-01', '2020-04-01'),
    formation_include: await Faker.lorem.paragraph

  }
})

Factory.blueprint('App/Models/Appointment', async (faker) => {
  const status = ['Andamento', 'Concluído', 'Agendada', 'Disponível'];
  const times =['10:40:00', '13:51:00', '10:14:00', '14:52:00', '15:32:00', '18:00:00']
  const payments = ['Aguardando', 'estornado', 'Pago', 'Concluído']
  return {
    status: await Faker.random.arrayElement(status),
    date : await Faker.date.between('2020-01-01', '2020-04-01'),
    hour : await Faker.random.arrayElement(times),
    payment_status: await Faker.random.arrayElement(payments),

  }
})


Factory.blueprint('App/Models/Attachment', async (faker) => {
  return {
      name: await Faker.name.firstName,
      path: await Faker.image.imageUrl,
      key: "key"
  }
})



Factory.blueprint('App/Models/BankAccount', async (faker) => {
  return {
    institution: await Faker.finance.accountName,
    agency: await Faker.finance.currencyCode,
    account: await Faker.random.number
  }
})


Factory.blueprint('App/Models/Doctor', async (faker) => {
  const status = [0 , 1]
  return {
    crm : await Faker.random.number,
    rqe : await Faker.random.number,
    status: await Faker.random.arrayElement(status)
  }
})


Factory.blueprint('App/Models/DoctorsExperience', async (faker) => {
  const periodo = ['01/2020 ', '01/2019', '03/2018 ', '02/2018 ', '12/2018 ', '11/2017 ']
  return {
    location : await Faker.address.streetName,
    start :  await Faker.random.arrayElement(periodo),
    end: await Faker.random.arrayElement(periodo)
  }
})

Factory.blueprint('App/Models/Evaluation', async (faker) => {
  const value = [1, 2, 3, 4, 5]
  return {
    value :  await Faker.random.arrayElement(value)
  }
})

Factory.blueprint('App/Models/Patient', async (faker) => {
  const weights = ['1.90', '1.65' ,'1.70' ,'1.75' ,'1.77' ,'1.79']
  const height = ['50', '60' ,'71' ,'79' ,'75' ,'80']
  return {
    weight :  await Faker.random.arrayElement(weights),
    height :  await Faker.random.arrayElement(height),
    question1 :  await Faker.lorem.paragraph,
    question2 :  await Faker.lorem.paragraph,
    question3 :  await Faker.lorem.paragraph
  }
})

Factory.blueprint('App/Models/Payment', async (faker) => {
  const status = ['Aguardando pagamento', 'estornado', 'Pago', 'Concluído']
  return {
    status :  await Faker.random.arrayElement(status),
    date :  await Faker.date.between('2020-01-01', '2020-04-01')
  }
})

Factory.blueprint('App/Models/Prescription', async (faker) => {
  return {
    description:  await Faker.lorem.paragraph
  }
})


Factory.blueprint('App/Models/Schedule', async (faker) => {
  const times =['10:40:00', '13:51:00', '10:14:00', '14:52:00', '15:32:00', '18:00:00']
  const week = ['SEGUNDA', 'TERCA', 'QUARTA', 'QUINTA', 'SEXTA', 'SABADO', 'DOMINGO']
  return {
    week_day:  await Faker.random.arrayElement(week),
    period_of:  await Faker.random.arrayElement(times),
    period_until:  await Faker.random.arrayElement(times),
    interval: await Faker.random.arrayElement(times)
  }
})

Factory.blueprint('App/Models/SchedulesHour', async (faker) => {
  const times =['10:40:00', '13:51:00', '10:14:00', '14:52:00', '15:32:00', '18:00:00']

  return {
    period_of:  await Faker.random.arrayElement(times),
    period_until:  await Faker.random.arrayElement(times)
  }
})


Factory.blueprint('App/Models/Specialty', async (faker) => {
  const specialities = ['Cardiologia', 'derivando', 'telecardiologia',
    'Pneumologia', 'derivando' , 'telepneumologia',
    'Neurologia', 'derivando' , 'teleneurologia']
  return {
    description:  await Faker.random.arrayElement(specialities)
  }
})
