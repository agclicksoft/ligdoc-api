'use strict'

/*
|--------------------------------------------------------------------------
| InitialSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class InitialSeeder {
  async run () {
    try {


    const users = await Factory
    .model('App/Models/User')
    .createMany(30)

     await Factory
    .model('App/Models/Specialty')
    .createMany(15)
    const associats = []
    for (const user of users) {

      if (user.role == 'DOCTOR') {


        const doctor = await Factory.model('App/Models/Doctor').make()
        await user.doctor().save(doctor)

        if (associats['DOCTOR'] === undefined){
          associats['DOCTOR'] = doctor
        }

        const academies = await Factory.model('App/Models/AcademicEducation').make()
        await doctor.academicsEducation().save(academies)

        const experiences = await Factory.model('App/Models/DoctorsExperience').make()
        await doctor.experiences().save(experiences)

        const schedules = await Factory.model('App/Models/Schedule').make()
        await doctor.schedules().save(schedules)

        const schedulesHours = await Factory.model('App/Models/SchedulesHour').make()
        await schedules.schedulesHours().save(schedulesHours)
      }

      if (user.role == 'PATIENT') {
        const patient = await Factory.model('App/Models/Patient').make()
        await user.patient().save(patient)

        if (associats['PATIENT'] === undefined){
          associats['PATIENT'] = patient
        }
      }

      if (associats['PATIENT'] !== undefined &&  associats['DOCTOR'] !== undefined) {
        //Cria compromisso
        const appointments = await Factory.model('App/Models/Appointment').createMany(2)
        for (const appointment of appointments) {
          await appointment.doctor().associate(associats['DOCTOR'])
          await appointment.patient().associate(associats['PATIENT'])
        }
        const evaluatuions = await Factory.model('App/Models/Evaluation').createMany(2)
        for (const evaluatuion of evaluatuions) {
          await evaluatuion.doctor().associate(associats['DOCTOR'])
          await evaluatuion.patient().associate(associats['PATIENT'])
        }

        associats['PATIENT'] = undefined
        associats['DOCTOR']= undefined
      }

     const banks = await Factory
      .model('App/Models/BankAccount')
      .create(1)

     await user.accounts().save(banks)

     const attachment = await Factory
      .model('App/Models/Attachment')
      .create(1)

     await user.attachments().save(attachment)


    }
    } catch (error) {
        console.log(error)
    }
  }

}

module.exports = InitialSeeder
