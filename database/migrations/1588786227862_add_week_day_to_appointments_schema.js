'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddWeekDayToAppointmentsSchema extends Schema {
  up () {
    this.table('appointments', (table) => {
      table.string('week_day', 20).after('date')
    })
  }

  down () {
    this.table('appointments', (table) => {
      table.dropCollum('week_day')
    })
  }
}

module.exports = AddWeekDayToAppointmentsSchema
