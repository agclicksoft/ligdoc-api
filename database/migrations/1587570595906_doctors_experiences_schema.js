'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DoctorsExperiencesSchema extends Schema {
  up () {
    this.create('doctors_experiences', (table) => {
      table.increments()
      table
      .integer('doctor_id ')
      .unsigned()
      .references('id')
      .inTable('doctors')
      table.string('location', 254).nullable()
      table.string('start', 10).nullable()
      table.string('end', 10).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('doctors_experiences')
  }
}

module.exports = DoctorsExperiencesSchema
