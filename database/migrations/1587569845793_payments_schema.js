'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentsSchema extends Schema {
  up () {
    this.create('payments', (table) => {
      table.increments()
      table
      .integer('patient_id')
      .unsigned()
      .references('id')
      .inTable('patients')
      table
      .integer('appointment_id')
      .unsigned()
      .references('id')
      .inTable('appointments')
      table.string('status', 80).nullable()
      table.date('date').nullable()


      table.timestamps()
    })
  }

  down () {
    this.drop('payments')
  }
}

module.exports = PaymentsSchema
