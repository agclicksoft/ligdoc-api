'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AppointmentsSchema extends Schema {
  up () {
    this.create('appointments', (table) => {
      table.increments()
      table
      .integer('doctor_id ')
      .unsigned()
      .references('id')
      .inTable('doctors')
      //.onUpdate('CASCADE')
      table
      .integer('patient_id ')
      .unsigned()
      .references('id')
      .inTable('patients')
      table.string('status', 50).nullable()
      table.date('date').nullable()
      table.time('hour', { precision: 4 })
      table.string('payment_status', 80).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('appointments')
  }
}

module.exports = AppointmentsSchema
