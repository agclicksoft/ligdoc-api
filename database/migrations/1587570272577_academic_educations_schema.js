'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AcademicEducationsSchema extends Schema {
  up () {
    this.create('academic_educations', (table) => {
      table.increments()
      table
      .integer('doctor_id ')
      .unsigned()
      .references('id')
      .inTable('doctors')
      table.string('institute_location', 254).nullable()
      table.string('institute_conclusion', 254).nullable()
      table.string('residence_location', 254).nullable()
      table.string('residence_conclusion', 254).nullable()
      table.string('formation_include', 6000).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('academic_educations')
  }
}

module.exports = AcademicEducationsSchema
