'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SpecialtiesSchema extends Schema {
  up () {
    this.create('specialties', (table) => {
      table.increments()
      table.string('description', 254).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('specialties')
  }
}

module.exports = SpecialtiesSchema
