'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddCnpjToDoctorSchema extends Schema {
  up () {
    this.table('doctors', (table) => {
      table.string('cnpj', 50).after('rqe')
    })
  }

  down () {
    this.table('doctors', (table) => {
      table.dropColumn('cnpj')
    })
  }
}

module.exports = AddCnpjToDoctorSchema
