'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddNumberPhoneToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('cellphone', 50).after('contact')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('cellphone')
    })
  }
}

module.exports = AddNumberPhoneToUserSchema
