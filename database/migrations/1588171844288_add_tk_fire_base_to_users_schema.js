'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddTkFireBaseToUsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('tk_firebase', 254).nullable().after('tk_payment')
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddTkFireBaseToUsersSchema
