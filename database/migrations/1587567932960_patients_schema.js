'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatientsSchema extends Schema {
  up () {
    this.create('patients', (table) => {
      table.increments()
      table.float('weight').nullable()
      table.float('height').nullable()
      table.string('health_condition', 6000).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('patients')
  }
}

module.exports = PatientsSchema
