'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddContraintToAppointmentsSchema extends Schema {
  up () {
    this.table('appointments', (table) => {
      table
      .integer('specialty_id')
      .unsigned()
      .references('id')
      .inTable('specialties')
      .after('patient_id')
    })
  }

  down () {
    this.table('appointments', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddContraintToAppointmentsSchema
