'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddZipCodeSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('address_zipcode', 10).nullable().after('genre')
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddZipCodeSchema
