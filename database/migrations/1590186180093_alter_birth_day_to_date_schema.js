'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterBirthDayToDateSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.date('birth_day').alter()
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterBirthDayToDateSchema
