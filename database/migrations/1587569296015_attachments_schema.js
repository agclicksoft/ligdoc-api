'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AttachmentsSchema extends Schema {
  up () {
    this.create('attachments', (table) => {
      table.increments()
      table
      .integer('user_id')
      .unsigned()
      .references('id')
      .inTable('users')
     // .onUpdate('CASCADE')
      table
      .integer('appointment_id')
      .unsigned()
      .references('id')
      .inTable('appointments')
      table.string('path', 80).nullable()
      table.string('key', 40).nullable()
      table.string('name', 40).nullable()
     // .onUpdate('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('attachments')
  }
}

module.exports = AttachmentsSchema
