'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EvaluationSchema extends Schema {
  up () {
    this.create('evaluations', (table) => {
      table.increments()
      table
      .integer('doctor_id ')
      .unsigned()
      .references('id')
      .inTable('doctors')
      //.onUpdate('CASCADE')
      table
      .integer('patient_id ')
      .unsigned()
      .references('id')
      .inTable('patients')
      //.onUpdate('CASCADE')
      table.float('value').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('evaluations')
  }
}

module.exports = EvaluationSchema
