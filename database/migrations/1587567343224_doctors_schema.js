'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DoctorsSchema extends Schema {
  up () {
    this.create('doctors', (table) => {
      table.increments()
      table
      .integer('user_id ')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      table.string('crm', 50).nullable()
      table.string('rge', 50).nullable()
      table.boolean('status').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('doctors')
  }
}

module.exports = DoctorsSchema
