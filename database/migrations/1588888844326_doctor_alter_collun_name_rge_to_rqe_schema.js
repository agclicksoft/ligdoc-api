'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DoctorAlterCollunNameRgeToRqeSchema extends Schema {
  up () {
    this.table('doctors', (table) => {
      table.renameColumn('rge', 'rqe')
    })
  }

  down () {
    this.table('doctors', (table) => {
      table.renameColumn('rqe', 'rge')
    })
  }
}

module.exports = DoctorAlterCollunNameRgeToRqeSchema
