'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('name', 60).notNullable()
      table.string('role', 60).notNullable()
      table.string('img_profile', 100).nullable()
      table.string('contact', 60).nullable()
      table.string('cpf', 60).nullable()
      table.string('genre', 10).nullable()
      table.string('birth_day', 11).nullable()
      table.string('address_street', 100).nullable()
      table.string('address_number', 30).nullable()
      table.string('address_complement', 200).nullable()
      table.string('address_city', 60).nullable()
      table.string('address_uf', 2).nullable()
      table.string('address_neighborhood', 60).nullable()
      table.string('tk_payment', 200).nullable()
      table.boolean('accept_terms').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
