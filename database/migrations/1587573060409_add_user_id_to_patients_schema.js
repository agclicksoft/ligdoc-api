'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddUserIdToPatientsSchema extends Schema {
  up () {
    this.table('patients', (table) => {
      table
      .integer('user_id')
      .unsigned()
      .references('id')
      .inTable('users')
      .after('id')
    })
  }

  down () {
    this.table('patients', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddUserIdToPatientsSchema
