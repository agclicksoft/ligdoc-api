'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddQuestionsToPatientSchema extends Schema {
  up () {
    this.table('patients', (table) => {
      table.dropColumn('health_condition')
      table.string('question1', 2000).after('height')
      table.string('question2', 2000).after('question1')
      table.string('question3', 2000).after('question2')
    })
  }

  down () {
    this.table('patients', (table) => {
      table.string('health_condition', 6000)
      table.dropColumn('question1')
      table.dropColumn('question2')
      table.dropColumn('question3')
    })
  }
}

module.exports = AddQuestionsToPatientSchema
