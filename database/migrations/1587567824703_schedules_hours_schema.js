'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SchedulesHoursSchema extends Schema {
  up () {
    this.create('schedules_hours', (table) => {
      table.increments()
      table
      .integer('schedule_id ')
      .unsigned()
      .references('id')
      .inTable('schedules')
      table.time('period_of', { precision: 4 })
      table.time('period_until', { precision: 4 })
   //   .onUpdate('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('schedules_hours')
  }
}

module.exports = SchedulesHoursSchema
