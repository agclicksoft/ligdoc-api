'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SchedulesSchema extends Schema {
  up () {
    this.create('schedules', (table) => {
      table.increments()
      table
      .integer('doctor_id ')
      .unsigned()
      .references('id')
      .inTable('doctors')
      .onUpdate('CASCADE')
      table.string('week_day', 50).nullable()
      table.time('period_of', { precision: 4 })
      table.time('period_until', { precision: 4 })
      table.time('interval', { precision: 4 })
      table.timestamps()
    })
  }

  down () {
    this.drop('schedules')
  }
}

module.exports = SchedulesSchema
