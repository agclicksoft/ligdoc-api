'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddAppointmentsToEvaluetionsSchema extends Schema {
  up () {
    this.table('evaluations', (table) => {
      table
      .integer('appointment_id ')
      .unsigned()
      .references('id')
      .inTable('appointments')
      .after('patient_id')
    })
  }

  down () {
    this.table('evaluations', (table) => {
      table.dropColumn('appointment_id')
    })
  }
}

module.exports = AddAppointmentsToEvaluetionsSchema
