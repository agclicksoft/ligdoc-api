'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PrescriptionsSchema extends Schema {
  up () {
    this.create('prescriptions', (table) => {
      table.increments()
      table
      .integer('appointment_id')
      .unsigned()
      .references('id')
      .inTable('appointments')
      table.string('description', 6000).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('prescriptions')
  }
}

module.exports = PrescriptionsSchema
