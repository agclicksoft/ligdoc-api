'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddDescriptionToEvaluetionSchema extends Schema {
  up () {
    this.table('evaluations', (table) => {
      table.string('description', 254).after('value')
    })
  }

  down () {
    this.table('evaluations', (table) => {
      table.dropColumn('description')
    })
  }
}

module.exports = AddDescriptionToEvaluetionSchema
