'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DoctorsSpecialtiesSchema extends Schema {
  up () {
    this.create('doctors_specialties', (table) => {
      table.increments()
      table
      .integer('doctor_id ')
      .unsigned()
      .references('id')
      .inTable('doctors')
      .onUpdate('CASCADE')
      table
      .integer('specialtie_id ')
      .unsigned()
      .references('id')
      .inTable('specialties')
      .onUpdate('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('doctors_specialties')
  }
}

module.exports = DoctorsSpecialtiesSchema
