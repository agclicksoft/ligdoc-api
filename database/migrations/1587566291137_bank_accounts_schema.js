'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BankAccountsSchema extends Schema {
  up () {
    this.create('bank_accounts', (table) => {
      table.increments()
      table
      .integer('user_id ')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      //   .onDelete('CASCADE')
      table.string('institution', 254).nullable()
      table.string('agency', 60).nullable()
      table.string('account', 60).nullable()

      table.timestamps()
    })
  }

  down () {
    this.drop('bank_accounts')
  }
}

module.exports = BankAccountsSchema
