'use strict'

const Specialty = use('App/Models/Specialty')
const Doctor = use('App/Models/Doctor')

class SpecialtyController {

  async index({ request }) {
    const { has } = request.only(['has'])
    const specialites = Specialty.query()
    if (has) {
      specialites.has(has)
    }
    return specialites.fetch()

  }

  async doctors({params, pagination}){

    const doctors = Doctor.query()
    .whereHas('specialties', (builder) => {
      builder.where('specialtie_id', params.id)
    })
    .paginate(pagination.page, pagination.perPage)

    return doctors
  }

}

module.exports = SpecialtyController
