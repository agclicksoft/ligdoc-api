
var PagSeguro = require('node-pagseguro2');

const Appointment = use('App/Models/Appointment')
const Payment = use('App/Models/Payment')
const Env = use('Env')
const OneSignalController = use('./OneSignalController.js')

var pagseguro = new PagSeguro({
  email: Env.get('PAG_MAIL'),
  token: Env.get('PAG_TOKEN'),
  sandbox: 1
});


class PagSeguroController {

  //notificationCode
  async statusNotification({ request, response }) {
    const { notificationCode } = request.only(['notificationCode'])
    //Para busca no pagseguro precisa ser uma promisse para aguardar o retorno
    var transaction = new Promise((resolve, reject) => {
      pagseguro.notificationStatus(notificationCode.toString().replace(/[^[:alnum:]-]/, ""), function (err, data) {
        if (!err) {
          resolve(data);
        } else {
          reject(err);
        }
      });
    })
    transaction.then(content => {
      //e dai salvar no banco
      this.saveTransaction(content)
      return response.status(200).send()
    })
    transaction.catch(content => {
      console.log(content)
    })
  }
  // //Função de teste
  // async novaTransacao({ response }) {
  //   pagseguro.addItem({
  //     id: '1',
  //     description: 'Descrição do primeiro produto',
  //     amount: '40.00',
  //     quantity: '1'
  //   });

  //   pagseguro.addItem({
  //     id: '2',
  //     description: 'Descrição do segundo produto',
  //     amount: '40.00',
  //     quantity: '9'
  //   });

  //   pagseguro.sender({
  //     name: 'Some name',
  //     email: 'name@example.com',
  //     phone: {
  //       areaCode: '51',
  //       number: '12345678'
  //     }
  //   });

  //   pagseguro.shipping({
  //     type: 1,
  //     name: 'Some name',
  //     email: 'name@example.com',
  //     address: {
  //       street: 'Endereço',
  //       number: '10',
  //       city: 'Nome da cidade',
  //       state: 'PR',
  //       country: 'BRA'
  //     }
  //   });

  //   var status = new Promise((resolve, reject) => {
  //     pagseguro.checkout(function (success, res) {
  //       if (success) {
  //         resolve(JSON.parse(convert.xml2json(res, { compact: true, spaces: 4 })));
  //       }
  //       else {
  //         reject(res);
  //       }
  //     });
  //   })

  //   await status.then(content => {
  //     return response.status(201).send("https://sandbox.pagseguro.uol.com.br/checkout/v2/payment.html?code=" + content.checkout.code._text)
  //   })
  // }

  async saveTransaction(result) {
    // Atualizando o status do pagamento
    console.log(result)    
    const findPayment = await Payment.findOrFail(result.reference)

    const statusDescription = {
      "1": "AGUARDANDO PAGAMENTO",
      "2": "EM ANALISE",
      "3": "PAGO",
      "4": "DISPONIVEL",
      "5": "EM DISPUTA",
      "6": "DEVOLVIDO",
      "7": "CANCELADO",
      "8": "DEBITADO",
      "9": "RETENCAO TEMPORARIA"
    }    
    findPayment.status = statusDescription[result.statuscode]
    await findPayment.save()

    //Atualizando a consulta proprimente
    const findAppointment = await Appointment.find(findPayment.appointment_id)
    findAppointment.status = statusDescription[result.statuscode]
    findAppointment.payment_status = statusDescription[result.statuscode]
    await findAppointment.save()

    if (findAppointment.status == 'PAGO') {
      console.log("Enviando notificações para médicos baseado no compromisso " + findAppointment.id)
      const pushNotification = new OneSignalController()
      pushNotification.sendNotification(findAppointment)
    }
  }

  async js({ response }) {
    return response.status(200).send(`
    <!DOCTYPE html>
    <html>
    <head>
        <title>Pag Seguro API</title>
        <script type="text/javascript"
            src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
    </head>
    <body>
    </body>
    </html>
    `)
  }

  async charge({ request, response }) {
    try {

      const {
        sender,
        shipping,
        item,
        method,
        credit_card_token,
        value,
        hash,
        reference,
      } = request.all();
      console.log('Efetuando checkout');

      var payment = new PagSeguro({
        email: 'ligdoc.telemedicina@gmail.com',
        token: '7DF8CF2A79A84C84B596E726C64208E7',
        sandbox: 1,
        sandbox_email: sender.email,
      });

      payment.setSender(sender);
      payment.setShipping(shipping);
      payment.addItem(item);

      const sendTransaction = () => new Promise((resolve, reject) =>
        payment.sendTransaction({
          method, //'boleto' ou 'creditCard'
          credit_card_token,
          value,
          // installments: Number, //opcional, padrão 1
          // extra_amount: Number, //opcional, padrão 0
          reference,
          hash,
        }, function (err, data) {
          if (err)
            reject(err);
          resolve(data);
        })
      );

      const transaction = await sendTransaction();
      console.log(transaction);
      return response.status(200).send(transaction);

    } catch (e) {
      console.log(e);
      return response.status(400).send(e);
    }
  }

}

module.exports = PagSeguroController
