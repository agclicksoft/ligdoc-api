'use strict'

const Database = use('Database')

class Dashboard {

  async index({ response }) {
    const countDoctors = await Database.table('doctors').count('* as doctors').first()
    const countPatients = await Database.table('patients').count('* as patients').first()

    return response.status(200).send( {...countDoctors, ...countPatients})
  }

}

module.exports = Dashboard
