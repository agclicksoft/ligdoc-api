'use strict'

const User = use('App/Models/User')
const Database = use('Database')
const Env = use('Env')


class AuthController {
  async register({ request, response }) {
    const trx = await Database.beginTransaction()

    try {
      const { name, email, password, role, accept_terms, crm, cellphone, specialties  } = request.all()

      const findUser = await User.findBy('email', email)
      if (findUser) {
        return response.status(409).send({data :'email já cadastrado'})
      }

      const user = await User.create({ name, email, password, role, accept_terms, cellphone }, trx)
      // commita a transaction
      await trx.commit()
      if (role == 'doctor') {
        const doctor = await user.doctor().create({status : 0, crm})
        await doctor.specialties().attach(specialties)
      }else {
        await user.patient().create({ question1: 'Não informado', question2: 'Não informado', question3: 'Não informado'})
      }
      return response.status(201).send({ data: user })
    } catch (e) {
      await trx.rollback()
      return response.status(400).send({
        error: 'Erro ao realizar cadastro',
        message: e.message
      })
    }
  }

  async login({ request, response, auth }) {
    const { email, password } = request.only(['email', 'password'])
    const { tk_firebase } = request.only(['tk_firebase'])
    let data = await auth.withRefreshToken().attempt(email, password)

    const user = await User.findByOrFail('email', email)
    if (tk_firebase) {
      user.tk_firebase = tk_firebase
      await user.save()
    }
    if (user.role == 'patient') {
      await user.load('patient')
    }
    if (user.role == 'doctor') {
      await user.load('doctor')
    }
    return response.send({ data, user })
  }


  async refresh({ request, response, auth }) {
    var refresh_token = request.input('refresh_token')
    console.log(request)

    if (!refresh_token) {
      refresh_token = request.header('refresh_token')
    }

    const data = await auth
      .newRefreshToken()
      .generateForRefreshToken(refresh_token)

    const user = await auth
      .getUser()

    return response.send({ data: data, user })
  }

  async logout({ request, response, auth }) {
    let refresh_token = request.input('refresh_token')

    if (!refresh_token) {
      refresh_token = request.header('refresh_token')
    }

    const loggedOut = await auth
      .authenticator('jwt')
      .revokeTokens([refresh_token], true)

    return response.status(204).send({})
  }

  async forgot({ request, response }) {
    const user = await User.findByOrFail('email', request.input('email'))
    const req = request
    try {
      /**
       * Invalida qualquer outro token que tenha sido gerado anteriormente
       */
      await PasswordReset.query()
        .where('email', user.email)
        .delete()

      /**
       * gera um novo token para reset da senha
       */
      const reset = await PasswordReset.create({ email: user.email })

      // Envia um novo e-mail para o Usuário, com um token para que ele possa alterar a senha
      await Mail.send(
        'emails.reset',
        { user, reset, referer: req.request.headers['referer'] },
        message => {
          message
            .to(user.email)
            .from(Env.get('DO_NOT_ANSWER_EMAIL'))
            .subject('Solicitação de Alteração de Senha')
        }
      )

      return response.status(201).send({
        message:
          'Um e-mail com link para reset foi enviado para o endereço informado!'
      })
    } catch (error) {
      return response.status(400).send({
        message: 'Ocorreu um erro inesperado ao executar a sua solicitação!'
      })
    }
  }

  async remember({ request, response }) {
    const reset = await PasswordReset.query()
      .where('token', request.input('token'))
      .where('expires_at', '>=', new Date())
      .firstOrFail()

    return response.send(reset)
  }

  async reset({ request, response }) {
    const { email, password } = request.all()
    const user = await User.findByOrFail('email', email)
    try {
      user.merge({ password })
      await user.save()
      /**
       * Invalida qualquer outro token que tenha sido gerado anteriormente
       */
      await PasswordReset.query()
        .where('email', user.email)
        .delete()
      return response.send({ message: 'Senha alterada com sucesso!' })
    } catch (error) {
      return response
        .status(400)
        .send({ message: 'Não foi possivel alterar a sua senha!' })
    }
  }
}

module.exports = AuthController
