'use strict'

const Appointment = use('App/Models/Appointment')

const Evaluation = use('App/Models/Evaluation')

const Patient = use('App/Models/Patient')

class AppointmentsController {
  async index({ request, pagination }) {
      const results = Appointment.query()

      return results.paginate(pagination.page, pagination.perpage)
  }
  async show ( { params } ) {
    const appointment = await Appointment.findOrFail(params.id)
    return appointment
  }

  async store({ request, auth, response }) {
    const user = await auth.getUser()
      try {
        const data = await user.patient().fetch()
        const patient = await Patient.find(data.id)
        const appointment = await patient.appointments().create(request.all())
        appointment.week_day = await this.weekdayDescription(appointment.date.substr(0,10))
        appointment.status = "ANDAMENTO"
        appointment.payment_status = "AGUARDANDO PAGAMENTO"
        await appointment.save()
        //Referencia para o pagseguro
        const reference = await appointment.payments().create({ status:  "AGUARDANDO PAGAMENTO", patient_id: appointment.patient_id})
        return response.status(200).send({reference : reference.id})
      } catch (e) {
        return response.status(400).send({
          error : 'Error ao cadastrar um novo compromisso na agenda.',
          message: e.message
      })
    }
  }

  async update({request, response, auth, params}) {
      const appointment = await Appointment.findOrFail(params.id)
      const user = await auth.getUser()

      if (user.role != 'admin' && user.role != 'doctor') {
        return response.status(401).send({message: 'invalid role'})
      }

      appointment.merge(request.all())

      appointment.save()

      return response.status(200).send({
        message: 'Compromisso salvo',
        data: appointment
      })
  }


  weekdayDescription(date) {
    console.log(date)
    var semana = ["DOMINGO", "SEGUNDA", "TERCA", "QUARTA", "QUINTA", "SEXTA", "SABADO"];
    var arr = date.split("-");
    var teste = new Date(arr[0], arr[1] - 1, arr[2]);
    var dia = teste.getDay();
    return semana[dia]
  }

}
module.exports = AppointmentsController
