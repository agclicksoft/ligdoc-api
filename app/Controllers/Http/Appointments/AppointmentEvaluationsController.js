'use strict'

const Appointment = use('App/Models/Appointment')

const Evaluation = use('App/Models/Evaluation')

class AppointmentEvaluationsController {

    async store({request, params, auth, response}) {
      const appointment = await Appointment.findOrFail(params.id)
      const user = await auth.getUser();
      const patient = await user.patient().fetch()
      const evaluation = await Evaluation.findBy('appointment_id', appointment.id)
      const {value, description} = request.all()
      if (! evaluation) {
        appointment.status = 'FINALIZADO'
        await appointment.save()

        const evaluationSaved = await appointment.evaluation().create({value, description, patient_id : patient.id, doctor_id: appointment.doctor_id })
        return response.status(201).send({
            message: 'Avaliação cadastrada com sucesso',
            data: evaluationSaved
          })
      }else {
        return response.status(409).send({
          message: 'Avaliação já cadastrada',
          data: evaluation
        })
      }

    }

    async update({request, params, auth, response}) {
      const appointment = await Appointment.findOrFail(params.appointment)
      const user = await auth.getUser();
      const patient = await user.patient().fetch()
      const evaluation = await Evaluation.findOrFail(params.id)
      const {value, description} = request.all();

      if (appointment.patient_id != patient.id && user.role != 'admin') return response.status(401).send({message: 'Usuário sem permissão para avaliar consulta'})

      evaluation.merge({value, description})
      evaluation.save()

      appointment.status = 'FINALIZADO'

      await appointment.save()

      return response.status(200).send({
        message: 'Avaliação salva com sucesso',
        data: evaluation
      })
    }
}

module.exports = AppointmentEvaluationsController
