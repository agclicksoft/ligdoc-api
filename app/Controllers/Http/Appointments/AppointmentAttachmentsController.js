'use strict'

const Appointment = use('App/Models/Appointment')
const Attachment = use('App/Models/Attachment')

const Drive = use('Drive')

class AppointmentAttachmentsController {

    async index({params}) {
      const appointment = await Appointment.findOrFail(params.appointment)
      const attachments = await Attachment.query()
      .where('appointment_id', appointment.id)
      .fetch()

      return attachments

    }

    async store({ request, params, response }) {
      var attachments = {}
      const appointment = await Appointment.findOrFail(params.appointment)
      request.multipart.file('attachments', {}, async file => {
        // set file size from stream byteCount, so adonis can validate file size
        file.size = file.stream.byteCount

        // catches validation errors, if any and then throw exception
        const error = file.error()
        if (error.message) {
          throw new Error(error.message)
        }
        const Key =`uploads/${(Math.random() * 100).toString()}-${file.clientName}`
        const ContentType = file.headers['content-type']
        const ACL = 'public-read'
        // upload file to s3
        const path = await Drive.put(Key, file.stream, {
          ContentType,
          ACL
        })

        attachments =  await appointment.attachments().create({
          name: file.clientName,
          path,
          key: Key
        })
      })
      await request.multipart.process()
      return response.status(201).send({
        message: 'upload de arquivo concluido',
        data: attachments
      })
    }

}

module.exports = AppointmentAttachmentsController
