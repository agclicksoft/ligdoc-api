'use strict'

const Drive = use('Drive')

class UserController {
  //Attachments
  async uploadFile({ request, response, auth }) {
    const user = await auth.getUser()
    // upload rules

      const validationOptions = {
        types: ['jpeg', 'jpg', 'png', 'pdf'],
        size: '5mb'
      }

      request.multipart.file('attachments', validationOptions, async file => {
        // set file size from stream byteCount, so adonis can validate file size
        file.size = file.stream.byteCount

        // catches validation errors, if any and then throw exception
        const error = file.error()
        if (error.message) {
          throw new Error(error.message)
        }
        const Key =`uploads/${(Math.random() * 100).toString()}-${file.clientName}`
        const ContentType = file.headers['content-type']
        const ACL = 'public-read'
        // upload file to s3
        const path = await Drive.put(Key, file.stream, {
          ContentType,
          ACL
        })

        const attachments =  await user.attachments().create({
          name: file.clientName,
          path,
          key: Key
        })
      })
      await request.multipart.process()
      return response.status(201).send({
        message: 'upload de arquivo concluido',
        data: attachments
      })

  }
  //Imagem profile
  async changeProfile({request, auth, response}) {
    try {
      const user = await auth.getUser()

      const validationOptions = {
        types: ['jpeg', 'jpg', 'png'],
        size: '2mb'
      }
      request.multipart.file('profile', validationOptions, async file => {
        // set file size from stream byteCount, so adonis can validate file size
        file.size = file.stream.byteCount

        // catches validation errors, if any and then throw exception
        const error = file.error()
        if (error.message) {
          throw new Error(error.message)
        }
        const Key =`profile/${user.id}`
        const ContentType = file.headers['content-type']
        const ACL = 'public-read'
        // upload file to s3
        const path = await Drive.put(Key, file.stream, {
          ContentType,
          ACL
        })

        user.img_profile = path
        await user.save()
      })
      await request.multipart.process()
      return response.status(200).send({
        message: 'Foto de perfil atualizada com sucesso',
        data : user
      })
    } catch (error) {
      return response.status(error.status).send({
        message: "Houve um erro ao fazer o upload da imagem",
        error : error
      })
    }
  }

}

module.exports = UserController
