"use strict";

// SomeController.js

const Helpers = use('Helpers')

const constants = require(Helpers.appRoot() + "/app/Controllers/constans.js")

var OneSignal = require("onesignal-node");
const Env = use("Env");
const Doctor = use("App/Models/Doctor");
const moment = use("moment");

const client = new OneSignal.Client(
  Env.get("ONESIGNAL_APIID"),
  Env.get("ONESIGNAL_APIKRY")
);

class OneSignalController {
  async sendNotification(findAppointment) {
    //Data do agendamento
    var day = ("00" + findAppointment.date.getDate() ).slice(-2)
    var month = ("00" + findAppointment.date.getMonth() ).slice(-2)
    var year = findAppointment.date.getFullYear()
    const strDate = `${year}-${month}-${day}`
    //dia da semana do agendamento
    const weekday = this.weekdayDescription(strDate);
    //Hora do agendamento
    const dateHoursAppontment = moment()
      .date(findAppointment.date.getDate())
      .month(findAppointment.date.getMonth())
      .year(findAppointment.date.getFullYear())
      .hour(parseInt(findAppointment.hour.substr(0, 2)))
      .minute(parseInt(findAppointment.hour.substr(3, 2)))
      .toDate();
    var seconds1 = dateHoursAppontment.getSeconds();
    var minutes1 = dateHoursAppontment.getMinutes();
    var hour1 = dateHoursAppontment.getHours();
    //Hora mais 20 minutos do agendamento
    const hoursMoreTwentyMinutes = moment(dateHoursAppontment)
      .add(constants.CONSULTATION_INTERVAL + constants.CONSULTATION_TIME, "minutes")
      .toDate();
    var seconds2 = hoursMoreTwentyMinutes.getSeconds();
    var minutes2 = hoursMoreTwentyMinutes.getMinutes();
    var hour2 = hoursMoreTwentyMinutes.getHours();

    console.log(`Busca de médicos baseado nos seguintes dados : \n
        id do compromisso : ${findAppointment.id} \n
        id da especialidade : ${findAppointment.specialty_id} \n
        hora do compromisso : ${hour1}:${minutes1}:${seconds1} \n
        hora2 do compromisso : ${hour2}:${minutes2}:${seconds2} \n
        data do compromisso : ${strDate} \n
        dia da semana do compromisso : ${weekday} \n
    `);

    const doctors = await Doctor.query()
      .where("status", true)
      .whereHas("specialties", (builder) => {
        builder.where("specialtie_id", findAppointment.specialty_id);
      })
      .whereHas("schedules", (builder) => {
        builder.andWhere(function () {
          this.where("period_of", "<=", `${hour1}:${minutes1}:${seconds1}`);
          this.where("period_until", ">=", `${hour2}:${minutes2}:${seconds2}`);
          this.where("week_day", weekday);
        });
      })
      .whereDoesntHave("appointments", (builder) => {
          builder.where("date", strDate);
          builder.whereBetween("hour", [
            `${hour1}:${minutes1}:${seconds1}`,
            `${hour2}:${minutes2}:${seconds2}`,
          ]);
      })
      .fetch();

    //PEGA TODOS TK'S DO USUARIO ASSOCIADO AO MEDICO E ENVIA NO PUSH NOTIFICATION
    const arrayTkFirebase = [];
    for (const doctor of doctors.rows) {
      const user = await doctor.user().fetch();
      if (user.tk_firebase) {
        arrayTkFirebase.push(user.tk_firebase);
      }
    }
    if (arrayTkFirebase.length == 0) {
      console.log(
        "Não existem médicos correspondentes ao paramentro do agendamento"
      );
      return;
    }

    const notification = {
      contents: {
        en: "Há um novo atendimento disponivel para confirmação",
      },
      include_player_ids: arrayTkFirebase,
      app_url: `app://ligdoc.telemedicina/accept-appointment=${appointment_id}`,
    };

    // using async/await
    try {
      const response = await client.createNotification(notification);
      console.log(response.body.id);
    } catch (e) {
      if (e instanceof OneSignal.HTTPError) {
        // When status code of HTTP response is not 2xx, HTTPError is thrown.
        console.log(e.statusCode);
        console.log(e.body);
      }
    }
  }
  weekdayDescription(date) {

    var semana = [
      "DOMINGO",
      "SEGUNDA",
      "TERCA",
      "QUARTA",
      "QUINTA",
      "SEXTA",
      "SABADO",
    ];
    var arr = date.split("-");
    var teste = new Date(arr[0], arr[1] - 1, arr[2]);
    var dia = teste.getDay();
    return semana[dia];
  }
}

module.exports = OneSignalController;
