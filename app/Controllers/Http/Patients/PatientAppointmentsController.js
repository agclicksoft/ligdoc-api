'use strict'
const moment = use('moment')
const Patient = use('App/Models/Patient')
const Appointment = use('App/Models/Appointment')
class PatientAppointmentsController {

  async reschedule({request, auth, response}) {
    const {appointment_id, date, hour} = request.only(['appointment_id', 'date', 'hour'])
    const appointment = await Appointment.findOrFail(appointment_id)

    const user = await auth.getUser()
    const patient = await user.patient().fetch()
    await Patient.findOrFail(patient.id)

    if (appointment.patient_id != patient.id) {
      return  response.status(401).send({message : "Paciente não pertence ao agendamento!"})
    }
    const nowMoreOne = moment().add(1, 'days')

    const dateHoursAppontment = moment()
    .date(appointment.date.getDate())
    .month(appointment.date.getMonth())
    .year(appointment.date.getFullYear())
    .hour(parseInt(appointment.hour.substr(0,2)))
    .minute(parseInt(appointment.hour.substr(3,2)))
    .toDate();

    if (nowMoreOne >= dateHoursAppontment) {
      return response.status(401).send({message : 'Não é possivel reagendar com menos de 24hrs de antecedência'})
    }

    appointment.merge({date, hour})

    appointment.save()

    return response.status(200).send({message : 'Consulta reagendada com sucesso!', data: appointment})

  }

}



module.exports = PatientAppointmentsController
