'use strict'

const Patient = use('App/Models/Patient')

const User = use('App/Models/User')

class PatientsController {

    async index( {pagination, request} ) {
      const {search} = request.only(['search'])
      const patients = Patient.query()
      .whereHas('user', (builder) => {
        if (search) {
          builder.where( function () {
            this
            .where('name', 'LIKE', `%${search}%`)
            .orWhere('email', 'LIKE',  `%${search}%`)
            .orWhere('cpf', 'LIKE',  `%${search}%`)
          })
        }
      })
      .with('user')
      return patients.paginate(pagination.page, pagination.perPage)
    }
    async show( {auth} ) {
      const user = await auth.getUser()
      const data = await user.patient().fetch()
      await Patient.findOrFail(data.id)
      const patient = await Patient.query()
        .where('id', data.id)
        .with('user', (builder) => {
           builder.with('accounts')
        })
        .with('appointments', (builder) => {
          builder.with('doctor', (builder) => {
            builder.with('user')
          })
          builder.with('payments')
          builder.with('specialty')
          builder.orderBy('id', 'desc')
        })
        .fetch()
      return patient.first()
    }

    async update({request, auth, params, response}) {
      const userAuth = await auth.getUser()
      const patient = await Patient.findOrFail(params.id)
      if (userAuth.id != patient.user_id && userAuth.role != 'admin') return response.status(401).send({message : "As credenciais não correspondem ao paciente"})
      // User
      const user = await User.findOrFail(patient.user_id)
      const {name, cpf, birth_day, genre, email, contact, cellphone, address_zipcode,
      address_street, address_number, address_complement, address_city, address_uf, address_neighborhood} = request.only(['user'])['user']

      user.merge({name, cpf, birth_day, genre, email, contact, cellphone, address_zipcode,
        address_street, address_number, address_complement, address_city, address_uf, address_neighborhood})
      await user.save()
      //Patient
      const {weight, height, question1, question2, question3 } = request.all()

      patient.merge({weight, height, question1, question2, question3})
      await patient.save()

      return patient
    }
}



module.exports = PatientsController
