'use strict'

const Helpers = use('Helpers')
const constants = require(Helpers.appRoot() + "/app/Controllers/constans.js")
const moment = use('moment')
const Schedule = use('App/Models/Schedule')
const Database = use('Database')
class SchedulesController {


  async index({ request }) {
      const { weekday } = request.only(['weekday'])
      const initialSearch = moment().hour(0).minute(0).second(0)
      const finalSearch = moment().hour(0).minute(constants.CONSULTATION_INTERVAL + constants.CONSULTATION_TIME).second(0)
      
      console.log(`iniciando busca por médicos ${initialSearch.format('HH:mm:ss')} <=> ${finalSearch.format('HH:mm:ss')}`)   


      if (weekday) {
        const data = []
        do {
          const results = await Database
          .select('period_of as start', 'period_until as end')
          .from('schedules')      
          .where('week_day', weekday)
          .andWhere('period_of', '>=', initialSearch.format('HH:mm:ss'))
          .andWhere('period_until', '<=', finalSearch.format('HH:mm:ss'))
          .limit(1)

          if (results.length > 0) {
            data.push({start: initialSearch.format('HH:mm:ss'), end: finalSearch.format('HH:mm:ss')})
          }
          initialSearch.add(constants.CONSULTATION_INTERVAL + constants.CONSULTATION_TIME, "minutes")
          finalSearch.add(constants.CONSULTATION_INTERVAL + constants.CONSULTATION_TIME, "minutes")
          
        } while (initialSearch.format("HH:mm:ss") != "00:00:00") {
          const results = await Database
          .select('period_of as start', 'period_until as end')
          .from('schedules')     
          .limit(1) 
          .where('week_day', weekday)
          .andWhere('period_of', '>=', initialSearch.format('HH:mm:ss'))
          .andWhere('period_until', '<=', finalSearch.format('HH:mm:ss'))

          if (results.length > 0) {
            data.push({start: initialSearch.format('HH:mm:ss'), end: finalSearch.format('HH:mm:ss')})
          }
          initialSearch.add(constants.CONSULTATION_INTERVAL + constants.CONSULTATION_TIME, "minutes")
          finalSearch.add(constants.CONSULTATION_INTERVAL + constants.CONSULTATION_TIME, "minutes")
        }
         return data
      }else {
        const results = Schedule.query()
        results.distinct('week_day')
        return results.fetch()
      }    

  }


}


module.exports = SchedulesController
