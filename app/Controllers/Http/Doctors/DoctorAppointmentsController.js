"use strict";
// SomeController.js
const Helpers = use('Helpers')
const Env = use('Env')
const constants = require(Helpers.appRoot() + Env.get("CONSTANT_PATH"))

const Doctor = use("App/Models/Doctor");
const Appointment = use("App/Models/Appointment");
const moment = use("moment");

class DoctorAppointmentsController {
  async index({ params, auth, response }) {
    const findDoctor = await Doctor.findOrFail(params.id);
    const user = await auth.getUser();
    if (user.id != findDoctor.user_id && user.role != 'admin')
      return response
        .status(401)
        .send({ message: "Usuário não corresponde ao médico" });
    //Buscar de todos Agendamentos que estejam pagos e não existem médicos associados ainda
    //E o periodo de disponibilidade do médico case com o horario do agendamento
    //DESATIVE OS COMENTARIOS ABAIXO PARA VER O LOG DE QUERY
    const schedules = await findDoctor.schedules().fetch();
    //A especialidade do appointment deve ser a especialidade do médico
    const specialties = await findDoctor.specialties().fetch();

    const idsSpecialties = specialties.rows.map(
      (specialty) => specialty.$attributes.id
    );

    //TODO : O appointiment não pode considir a data e hora com outro appointiment do médico
    const doctorAppointments = await Appointment.query()
      .where("doctor_id", findDoctor.id)
      .andWhere("date", ">=", moment().format("YYYY-MM-DD"))
      .fetch();

    const dateHours = doctorAppointments.rows.map(function (appointment) {
      return {
        date: moment(appointment.$attributes.date).format("YYYY-MM-DD"),
        hour: appointment.$attributes.hour,
        hourMoreTwenty: moment(appointment.$attributes.hour, "HH:mm:ss")
          .add(constants.CONSULTATION_INTERVAL + constants.CONSULTATION_TIME, "minutes")
          .format("HH:mm:ss"),
      };
    });

    const appointmets = Appointment.query()
      .where("status", "PAGO")
      .where("date", ">=", moment().format("YYYY-MM-DD"))
      .whereIn("specialty_id", idsSpecialties)
      .doesntHave("doctor") // Não existe médico associado ainda
      .where((builder) => {
        //console.log(' status = PAGO e ')
        for (const key in schedules.rows) {
          //if (key != 0) console.log(' OU ')
          builder.orWhere(function () {
            this.where("hour", "<=", schedules.rows[key].period_until);
            this.where("hour", ">=", schedules.rows[key].period_of);
            this.where("week_day", schedules.rows[key].week_day);
          });
          /* console.log(
            " hora >= " + schedules.rows[key].period_of
            + " e <= " +  schedules.rows[key].period_until
            + " e dia da semana = " + schedules.rows[key].week_day
        )*/
        }
      })
      //console.log('que não trabalhe')
      .whereNot((builder) => {
        //console.log('nos seguintes dias e horas')
        dateHours.forEach((dateHour) => {
          // A cada loop um novo OR
          builder.orWhere(function () {
            this.where("date", dateHour.date);
            this.andWhereBetween("hour", [dateHour.hour, dateHour.hourMoreTwenty]);
          });
        });
      })
      .fetch();

    return appointmets;
  }

  async store({ request, response, params, auth }) {
    const findDoctor = await Doctor.findOrFail(params.id);
    const user = await auth.getUser();
    if (user.id != findDoctor.user_id)
      return response
        .status(401)
        .send({ message: "Usuário não corresponde ao médico" });
    const { id } = request.only(["id"]);
    const appointment = await Appointment.findOrFail(id);

    if (!appointment.doctor_id) {
      appointment.doctor_id = findDoctor.id;
      await appointment.save();
      return response.status(200).send({
        message: "Médico associado ao agendamento",
        data: appointment,
      });
    } else {
      return response
        .status(401)
        .send({
          message: "Não é possível atribuir mais de uma médico ao agendamento",
        });
    }
  }

  async update({ request, auth, params, response }) {
    const findDoctor = await Doctor.findOrFail(params.doctor);
    const user = await auth.getUser();
    if (user.id != findDoctor.user_id)
      return response
        .status(401)
        .send({ message: "Usuário não corresponde ao médico" });

    const { date, hour } = request.only(["date", "hour"]);
    const appointment = await Appointment.findOrFail(params.id);

    if (appointment.doctor_id != findDoctor.id) {
      return response
        .status(401)
        .send({ message: "Médico não pertence ao agendamento!" });
    }
    const nowMoreOne = moment().add(12, "hours");

    const dateHoursAppontment = moment()
      .date(appointment.date.getDate())
      .month(appointment.date.getMonth())
      .year(appointment.date.getFullYear())
      .hour(parseInt(appointment.hour.substr(0, 2)))
      .minute(parseInt(appointment.hour.substr(3, 2)))
      .toDate();

    if (nowMoreOne >= dateHoursAppontment) {
      return response
        .status(401)
        .send({
          message:
            "Não é possivel reagendar com menos de 12hrs de antecedência",
        });
    }

    appointment.merge({ date, hour });

    appointment.save();

    return response
      .status(200)
      .send({ message: "Consulta reagendada com sucesso!", data: appointment });
  }
}

module.exports = DoctorAppointmentsController;
