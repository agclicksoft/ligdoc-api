'use strict'

const Doctor = use('App/Models/Doctor')

const Schedule = use('App/Models/Schedule')

class DoctorSchedulesController {

  async store({ request, params, response, auth }) {

    const doctor = await Doctor.findOrFail(params.id)
    const user = await auth.getUser()
    if ( user.id != doctor.user_id ) return response.status(401).send({message : 'Usuário não corresponde ao médico' })

    const {schedules} = request.only(['schedules'])
    //Deleta tudo antes salvar novamente
    await Schedule.query().where('doctor_id', doctor.id).delete()

    const schedulesCreat =  await doctor.schedules().createMany(schedules)

    return response.status(201).send({
      message: 'Agenda cadastrada com sucesso',
      data : schedulesCreat
    })
  }
}

module.exports = DoctorSchedulesController
