'use strict'

const Doctor = use('App/Models/Doctor')

const AcademicEducation = use('App/Models/AcademicEducation')

const DoctorsExperience = use('App/Models/DoctorsExperience')

const User = use('App/Models/User')

const BankAccount = use('App/Models/BankAccount')

const Database = use('Database')

class DoctorsController {


  async index({ pagination, request }) {
    var {orders} = request.only(['orders'])
    const {search} = request.only(['search'])

    const doctors = Doctor.query()
    .whereHas('user', (builder) => {
      if (search) {
        builder.where( function () {
          this
          .where('name', 'LIKE', `%${search}%`)
          .orWhere('email', 'LIKE',  `%${search}%`)
        })
      }
    })

    .with('user')
    .with('specialties')
    if ( search ) {
      doctors.orWhere('crm', 'LIKE', `%${search}%`)
    }
    if (orders) {
      orders = orders.split(',')
      doctors.orderBy(orders[0], orders[1])
    }

    return doctors.paginate(pagination.page, pagination.perPage)
  }

  async store({request, auth, response}) {
  /// Securty
    const admin = await auth.getUser()
    if (admin.role != 'admin') return response.status(401).send({message: 'Função apenas para administradores'})

    // User
    const {name, cpf, birth_day, genre, email, contact, cellphone, address_zipcode,
      address_street, address_number, address_complement, address_city, address_uf, address_neighborhood, password, accounts} = request.only(['user'])['user']

    const userOpt = await User.findBy('email', email)
    if (userOpt) return response.status(409).send({message: 'Email já cadastrado'})

    const user = await User.create({name, cpf, birth_day, genre, email, contact, cellphone, address_zipcode,
        address_street, address_number, address_complement, address_city, address_uf, role : 'doctor',
        accept_terms: 0, password, address_neighborhood })
    //doctor
    const {crm, specialties, rqe, cnpj} = request.all()
    const doctor = await user.doctor().create({ crm, rqe, cnpj, status: 1 })

    if (accounts) {
      await user.accounts().createMany(accounts)
    }
    //Specialties
    await doctor.specialties().attach(specialties)
      //AcademicEducation
    const {academicsEducation} = request.only(['academicsEducation'])

    await doctor.academicsEducation().createMany(academicsEducation)

    //Experiences
    const {experiences} = request.only(['experiences'])

    await doctor.experiences().createMany(experiences)

    return response.status(201).send({
        message: 'Médico salvo com sucesso'
    })

  }

  async show({ response, auth, params, request }) {
      const findDoctor = await Doctor.findOrFail(params.id)
      const user = await auth.getUser()
      await user.load('doctor')      
      //Se não for admin retorna o médico logado
      const doctorId = user.role == 'admin' || user.role == 'patient' ? params.id : user.$relations.doctor.id

      const doctor = await Doctor.query()
      .where('id', doctorId)
      .with('user', (builder) => {
        builder.with('attachments')
        builder.with('accounts')
      })
      .with('appointments', (builder) => {
        builder.orderBy('id', 'desc')
        builder.with('patient', (builder) => {
          builder.with('user')
        })
        builder.with('payments')
        builder.with('specialty')
      })
      .with('schedules')
      .with('specialties')
      .with('experiences')
      .with('academicsEducation')
      .first()
      //Avg evaluations
      const evaluations = await Database.from('evaluations').avg('value as evaluations').where('doctor_id', findDoctor.id).first()

      return response.status(200).send({...doctor.$attributes, ...doctor.$relations, ...evaluations})
  }

    async update({request, auth, params, response}) {
      const userAuth = await auth.getUser()
      const doctor = await Doctor.findOrFail(params.id)
      if (userAuth.id != doctor.user_id && userAuth.role != 'admin') return response.status(401).send({message : "Usuário não corresponde ao médico"})
      // User
      const user = await User.findOrFail(doctor.user_id)

      const {name, cpf, birth_day, genre, email, contact, cellphone, address_zipcode,
      address_street, address_number, address_complement, address_city, address_uf, address_neighborhood, accounts} = request.only(['user'])['user']

      user.merge({name, cpf, birth_day, genre, email, contact, cellphone, address_zipcode,
        address_street, address_number, address_complement, address_city, address_uf, address_neighborhood})
      await user.save()

      //doctor
      const {crm, specialties, rqe, cnpj } = request.all()
      doctor.merge({
        crm, rqe, cnpj
      })
      doctor.save()
      await BankAccount.query().where('user_id', user.id).delete()
      await user.accounts().createMany(accounts)
      //Specialties
      await doctor.specialties().detach()

      await doctor.specialties().attach(specialties)
      //AcademicEducation
      const {academicsEducation} = request.only(['academicsEducation'])

      await AcademicEducation.query().where('doctor_id', doctor.id).delete()

      await doctor.academicsEducation().createMany(academicsEducation)

      //Experiences
      const {experiences} = request.only(['experiences'])

      await DoctorsExperience.query().where('doctor_id', doctor.id).delete()

      await doctor.experiences().createMany(experiences)

      return response.status(200).send({
        message: 'Dados salvos',
        data: doctor
      })




    }


}


module.exports = DoctorsController
