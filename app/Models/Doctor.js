'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Doctor extends Model {
  academicsEducation() {
    return this.hasMany('App/Models/AcademicEducation')
  }
  appointments() {
    return this.hasMany('App/Models/Appointment')
  }
  user() {
    return this.belongsTo('App/Models/User')
  }
  experiences() {
    return this.hasMany('App/Models/DoctorsExperience')
  }

  specialties() {
    return this.belongsToMany('App/Models/Specialty', 'doctor_id', 'specialtie_id' )
    .pivotTable('doctors_specialties')
  }

  specialtiesDoctor() {
    return this.hasMany('App/Models/DoctorsSpecialty')
  }
  evaluations() {
    return this.hasMany('App/Models/Evaluation')
  }
  schedules() {
    return this.hasMany('App/Models/Schedule')
  }

}

module.exports = Doctor
