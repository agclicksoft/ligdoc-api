'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Evaluation extends Model {
  doctor () {
    return this.belongsTo('App/Models/Doctor')
  }
  patient () {
    return this.belongsTo('App/Models/Patient')
  }
  appointment () {
    return this.belongsTo('App/Models/Appointment')
  }

}

module.exports = Evaluation
