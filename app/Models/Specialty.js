'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Specialty extends Model {
  doctors() {
    return this.hasMany('App/Models/DoctorsSpecialty', "id", "specialtie_id")
  }
  appointments () {
    return this.hasMany('App/Models/Appointments')
  }
}

module.exports = Specialty
