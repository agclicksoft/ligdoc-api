'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Appointment extends Model {
  doctor () {
    return this.belongsTo('App/Models/Doctor')
  }
  patient () {
    return this.belongsTo('App/Models/Patient')
  }
  evaluation () {
    return this.hasOne('App/Models/Evaluation')
  }
  specialty () {
    return this.belongsTo('App/Models/Specialty')
  }
  payments () {
    return this.hasMany('App/Models/Payment')
  }
  prescription () {
    return this.hasOne('App/Models/Prescription')
  }
  attachments () {
    return this.hasMany('App/Models/Attachment')
  }
//Get and set
static get dates() {
  return super.dates.concat(["date"]);
}
static castDates(field, value) {
  if (field == "date") return value ? value.format("YYYY-MM-DD") : value;
  else return value ? value.format("YYYY-MM-DD hh:mm:ss") : value;
  // else used for created_at / updated_at
  }
}

module.exports = Appointment
