'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Patient extends Model {
  user() {
    return this.belongsTo('App/Models/User')
  }
  appointments() {
    return this.hasMany('App/Models/Appointment')
  }
  evaluations() {
    return this.hasMany('App/Models/Evaluation')
  }
  payments () {
    return this.hasMany('App/Models/Payment')
  }
}

module.exports = Patient
