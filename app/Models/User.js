'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {
  static boot() {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/Token')
  }

  attachments() {
    return this.hasMany('App/Models/Attachment')
  }

  accounts() {
    return this.hasMany('App/Models/BankAccount')
  }

  doctor() {
    return this.hasOne('App/Models/Doctor')
  }

  patient() {
    return this.hasOne('App/Models/Patient')
  }
  //Get and set
  static get dates() {
    return super.dates.concat(["birth_day"]);
  }
  static castDates(field, value) {
    if (field == "birth_day") return value ? value.format("YYYY-MM-DD") : value;
    else return value ? value.format("YYYY-MM-DD hh:mm:ss") : value;
    // else used for created_at / updated_at
  }


}

module.exports = User
