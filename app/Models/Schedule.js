'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Schedule extends Model {
  doctor () {
    return this.belongsTo('App/Models/Doctor')
  }
  schedulesHours () {
    return this.hasMany('App/Models/SchedulesHour')
  }
}

module.exports = Schedule
