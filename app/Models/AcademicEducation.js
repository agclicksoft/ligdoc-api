'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AcademicEducation extends Model {
    doctor () {
        return this.belongsTo('App/Models/Doctor')
    }

}

module.exports = AcademicEducation
