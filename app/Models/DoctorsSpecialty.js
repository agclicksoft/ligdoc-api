'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class DoctorsSpecialty extends Model {
  doctor () {
    return this.belongsTo('App/Models/Doctor')
  }
  specialty () {
    return this.belongsTo('App/Models/Specialty')
  }
}

module.exports = DoctorsSpecialty
