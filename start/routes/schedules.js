'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  /**
   * Schedules
   */
  Route.resource('schedules', 'SchedulesController').apiOnly().middleware('auth')


})
  .namespace('Schedules')
  .prefix('v1')
