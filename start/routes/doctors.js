'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  /**
   * Doctors
   */
  Route.resource('doctors', 'DoctorsController').apiOnly().middleware('auth')
 /**
   * Doctors schedules
   */
  Route.resource('doctors/:id/schedules', 'DoctorSchedulesController').apiOnly().middleware('auth')
 /**
   * Doctors appointments
   */
  Route.resource('doctors/:id/appointments', 'DoctorAppointmentsController').except(['update']).apiOnly().middleware('auth')
  Route.put('doctors/:doctor/appointments/:id', 'DoctorAppointmentsController.update')
})
  .namespace('Doctors')
  .prefix('v1')
