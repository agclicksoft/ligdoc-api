'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  /**
   * Specialties
   */
  Route.get('specialties', 'SpecialtyController.index')


})
  .namespace('Specialties')
  .prefix('v1')
