"use strict";

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.group(() => {
  Route.get('dashboard', 'DashboardController.index').middleware('auth')
})
  .prefix("v1")
  .namespace("Reports");
