'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  /**
   * Appointments
   */
  Route.resource('appointments', 'AppointmentsController').apiOnly().middleware('auth')
  /**
   * Appointments assessment
   */
  Route.resource('appointments/:id/evaluations', 'AppointmentEvaluationsController').except(['update']).apiOnly().middleware('auth')
  Route.put('appointments/:appointment/evaluations/:id', 'AppointmentEvaluationsController.update').middleware('auth')
  /**
   * Appointments attachments
   */
  Route.resource('appointments/:appointment/attachments', 'AppointmentAttachmentsController').except(['update']).apiOnly().middleware('auth')

})
  .namespace('Appointments')
  .prefix('v1')

   /**
   * PagSeguro - alteração de status fornecido pelo notification do pagseguro
   */
  Route.post('pagseguro', 'PagSeguroController.statusNotification')
  Route.get('pagseguro', 'PagSeguroController.novaTransacao')
  Route.get('pagseguro/js', 'PagSeguroController.js')
  Route.post('pagseguro/charge', 'PagSeguroController.charge')
