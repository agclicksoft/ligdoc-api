'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  /**
   * Users
   */
  //Route.resource('users', 'UserController').apiOnly().middleware('auth')
  Route.post('users/attachments', 'UserController.uploadFile').middleware('auth')
  Route.post('users/profile', 'UserController.changeProfile').middleware('auth')

})
  .namespace('Users')
  .prefix('v1')
