'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  /**
   * Products
   */
  Route.resource('patients', 'PatientsController').apiOnly().middleware('auth')

  Route.post('patients/reschedule', 'PatientAppointmentsController.reschedule').middleware('auth')
  //Route.get('specialties/:id/doctors', 'SpecialtyController.doctors')

})
  .namespace('Patients')
  .prefix('v1')

  Route.post('push', 'OneSignalController.sendNotification')
