'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')



/**
 * Import Auth Routes
 */
require('./auth')

/**
 * Import Auth Doctors
 */
require('./doctors')
/**
 * Import Auth Patients
 */
require('./patients')
/**
 * Import Auth Specialties
 */
require('./specialties')
/**
 * Import Auth Patients
 */
require('./schedules')
/**
 * Import Auth Appointments
 */
require('./appointments')
/**
 * Import Auth Users
 */
require('./users')
/**
 * Import Auth Admin Reports
 */
require('./reports')

